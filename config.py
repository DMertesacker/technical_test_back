import logging
import os

logging.basicConfig(level=logging.DEBUG)
basedir = os.path.abspath(os.path.dirname(__file__))

DB_USER = 'root'
DB_PSWD = ''
DB_HOST = '127.0.0.1'
DB_SCHEMA = 'algorath'

class Config:
	SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URI', 'mysql+pymysql://%s:%s@%s/%s' % (
		os.getenv('SQL_DB_USER', DB_USER),
		os.getenv('SQL_DB_PASSWORD', DB_PSWD),
		os.getenv('SQL_DB_HOST', DB_HOST),
		os.getenv('SQL_DB_SCHEMA', DB_SCHEMA)
	))

	SQLALCHEMY_DATABASE_URI_TEST = os.getenv('DATABASE_URI', 'mysql+pymysql://%s:%s@%s/%s' % (
		os.getenv('SQL_DB_USER', DB_USER),
		os.getenv('SQL_DB_PASSWORD', DB_PSWD),
		os.getenv('SQL_DB_HOST', DB_HOST),
		os.getenv('SQL_DB_SCHEMA', 'test')
	))

	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SQLALCHEMY_COMMIT_ON_TEARDOWN = False
	JSON_SORT_KEYS = False
