from werkzeug.exceptions import InternalServerError

from app.database.user.daos.user import UserDAO
from app.database.user.daos.user_relationship import UserRelationshipDAO
from app.decorators.transactional import transactional
from .translator import Translator


class UserServices:

	def __init__(self):
		# DAOs
		self.dao = UserDAO()
		self.rel_dao = UserRelationshipDAO()

		self.__load_translator()

	def __load_translator(self):
		self.translator = Translator()

	def _build_response_api(self, user_entity):
		id_user = user_entity.id_user
		rel_ids = self.rel_dao.get_relationships_by_user(id_user)

		return dict(
			userID=id_user, name=user_entity.name, relIDs=rel_ids,
			relNames=self.translator.username_translate(*rel_ids, only_list=True)
		)

	@transactional
	def _create_user(self, name=None):
		new_entity = self.dao.create_instance()
		new_entity.name = name
		self.dao.session.add(new_entity)

	@transactional
	def _create_relationships(self, id_user=None, *rels):
		self.rel_dao.add_relationship(id_user, *rels)

	def get_users(self, id_user=None):
		entities = self.dao.get_all() if id_user is None else [self.dao.get_by_pk(id_user)]

		res = []
		for e in entities:
			res.append(self._build_response_api(e))
		return res

	def get_user_options(self):
		entities = self.dao.get_all()

		return [dict(
			label=e.name, value=e.id_user
		) for e in entities]

	def create_user(self, name=None, rels=None):
		if not name:
			raise Exception('Need a Name to create a user')

		self._create_user(name)

		id_user = self.dao.get(first=True, name=name).id_user
		if rels:
			self._create_relationships(id_user, *rels)

		# Reload translator
		self.__load_translator()

		return self.get_users(id_user)

	@transactional
	def update_user(self, id_user=None, name=None, rels=None):
		if not id_user:
			raise Exception('User ID Not found')

		entity = self.dao.get_by_pk(id_user)
		if name:
			entity.name = name
			self.dao.session.add(entity)

		self.rel_dao.delete_all_relationships(id_user)
		if len(rels) > 0:
			self.rel_dao.add_relationship(id_user, *rels)

	@transactional
	def delete_user(self, id_user=None):
		if not id_user:
			raise Exception('User ID Not found')

		self.rel_dao.delete_all_relationships(id_user)
		self.dao.delete_users(id_user)
		self.__load_translator()

	def calculate_statistics(self):
		entities, entities_without_rels = self.dao.get_num_users_with_rels()

		entities_top_three = self.rel_dao.get_top_three_users_with_more_rels()
		return dict(
			userWithRels=entities[0].id_user,
			userWithoutRels=entities_without_rels[0].id_user,
			top=[dict(
				name=self.translator.username_translate(e.id_user),
				num=e.num_rel
			) for e in entities_top_three]
		)


