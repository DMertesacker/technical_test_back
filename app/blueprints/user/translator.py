from app.database.user.daos.user import UserDAO


class Translator:

	def __init__(self):
		self.user_dao = UserDAO()

		self._user_data_id = None


	def username_translate(self, *id_users, only_list=False):
		if self._user_data_id is None:
			self._user_data_id = dict((e.id_user, e.name) for e in self.user_dao.get_all())

		res = [self._user_data_id.get(id_user, id_user) for id_user in id_users]
		if len(res) == 1 and not only_list:
			return res[0]
		return res