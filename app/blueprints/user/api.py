from flask import jsonify, request

from app.blueprints.user import user
from .services import UserServices


services = UserServices()


@user.route('/all-users')
def api_users_get_all():
	res = services.get_users()
	return jsonify(res)


@user.route('/user-options')
def api_user_options():
	res = services.get_user_options()
	return jsonify(res)


@user.route('/new', methods=['POST'])
def api_user_new():
	data = request.get_json()
	res = services.create_user(**data)
	return jsonify(res)


@user.route('/update', methods=['POST'])
def api_user_update():
	data = request.get_json()
	services.update_user(**data)
	res = services.get_users(id_user=data['id_user'])
	return jsonify(res)


@user.route('/delete', methods=['DELETE'])
def api_user_delete():
	data = request.get_json()
	services.delete_user(**data)
	return jsonify(dict(ok=200))


@user.route('/statistics')
def api_user_statistics():
	data = services.calculate_statistics()
	return jsonify(data)