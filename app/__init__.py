from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

from app import blueprints
from config import Config

db = SQLAlchemy()


def create_app(config_class=Config):
	app = Flask(__name__)
	app.config.from_object(config_class)

	db.init_app(app)

	# Register all Blueprints
	blueprints.register_blueprints(app)

	CORS(app)
	return app


app = create_app()

@app.shell_context_processor
def make_shell_context():
	return {'db': db}