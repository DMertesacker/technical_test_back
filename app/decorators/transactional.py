from functools import wraps

from app import db


def transactional(func):
	@wraps(func)
	def deco_transaction(*args, **kwargs):
		try:
			res = func(*args, **kwargs)
			db.session.commit()
			return res

		except Exception as e:
			db.session.rollback()
			raise e
		finally:
			db.session.close()
			db.session.remove()

	return deco_transaction