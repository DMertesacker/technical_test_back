from app import db
from app.database.base_model import BaseModel


class UserRelationship(db.Model, BaseModel):
	__tablename__ = 'USER_RELATIONSHIP'

	id_user = db.Column(db.Integer, primary_key=True)
	user_relationship = db.Column(db.Integer, primary_key=True)



