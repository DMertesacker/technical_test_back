from app import db
from app.database.base_model import BaseModel


class User(db.Model, BaseModel):
	__tablename__ = 'USER'

	id_user = db.Column(db.Integer, primary_key=True, autoincrement=True)
	name = db.Column(db.String(100), nullable=False)



