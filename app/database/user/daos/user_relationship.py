from sqlalchemy import func

from app.database.base_dao import BaseDAO
from ..models.user_relationship import UserRelationship


class UserRelationshipDAO(BaseDAO):

	@property
	def model(self):
		return UserRelationship

	def get_all_users_with_rel(self):
		entities = self.session.query(
			self.model.id_user.distinct().label('id_user')
		)

		return [e.id_user for e in entities]

	def get_relationships_by_user(self, id_user):
		entities = self.get(id_user=id_user)
		return [rel.user_relationship for rel in entities]

	def add_relationship(self, id_target, *id_relationships):
		for rel in id_relationships:
			entity = self.create_instance()
			entity.id_user = id_target
			entity.user_relationship = rel
			self.session.add(entity)

	def delete_all_relationships(self, id_user):
		entities = self.get(id_user=id_user)
		for e in entities:
			self.session.delete(e)

	def get_top_three_users_with_more_rels(self):
		function = func.count(self.model.user_relationship).label('num_rel')

		return self.session.query(
			self.model.id_user, function
		).group_by(self.model.id_user)\
			.order_by(function.desc())\
			.limit(3)\
			.all()