from sqlalchemy import func

from app.database.base_dao import BaseDAO
from app.database.user.daos.user_relationship import UserRelationshipDAO
from ..models.user import User


class UserDAO(BaseDAO):

	@property
	def model(self):
		return User

	def get_id_user_by_names(self, *names):
		entities = self.model.query.filter(self.model.name.in_(*names))
		return [e.id_user for e in entities]

	def add_users(self, *list_name):
		for name in list_name:
			entity = self.create_instance()
			entity.name = name
			self.session.add(entity)

	def delete_users(self, *id_users):
		for id_user in id_users:
			entity = self.get_by_pk(id_user)
			if entity:
				self.session.delete(entity)

	def get_num_users_with_rels(self):
		id_user_rel = UserRelationshipDAO().get_all_users_with_rel()

		entities = self.session.query(
			func.count(self.model.id_user).label('id_user')
		).filter(self.model.id_user.in_(id_user_rel)).all()

		entities_without_rels = self.session.query(
			func.count(self.model.id_user).label('id_user')
		).filter(self.model.id_user.notin_(id_user_rel))

		return entities, entities_without_rels

