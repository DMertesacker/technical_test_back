import logging

from app import app, db
from app.database.user.daos.user import UserDAO
from app.database.user.daos.user_relationship import UserRelationshipDAO
from app.decorators.transactional import transactional


def create_default_db():
	__create_tables()
	__insert_default_user_data()
	__insert_default_relationships_data()


@transactional
def __create_tables():
	logging.info('>>>>>>>>>> INIT CREATE ALL DB <<<<<<<<<<')
	app.app_context().push()
	db.create_all()
	logging.info('>>>>>>>>>> FINISH CREATE ALL DB <<<<<<<<<<')


@transactional
def __insert_default_user_data():
	logging.info('>>>>>>>>>> INIT INSERT USERS IN DB <<<<<<<<<<')
	users = ['Ada', 'Bill', 'James', 'Richard', 'Guido', 'John', 'Alan', 'Dennis', 'Fred', 'Tim']
	UserDAO().add_users(*users)
	logging.info('>>>>>>>>>> FINISHED <<<<<<<<<<')


@transactional
def __insert_default_relationships_data():
	logging.info('>>>>>>>>>> INSERT USERS RELATIONSHIPS IN DB <<<<<<<<<<')
	data = [
		dict(target='Ada', relationships=['Bill', 'Richard']),
		dict(target='James', relationships=['Bill', 'Dennis', 'Fred']),
		dict(target='John', relationships=['Bill']),
		dict(target='Guido', relationships=['Bill', 'Alan', 'Ada'])
	]

	user_dao = UserDAO()
	dao = UserRelationshipDAO()
	for d in data:
		entity = user_dao.get(name=d['target'], first=True)
		if entity:
			ids_relationships = user_dao.get_id_user_by_names(d['relationships'])
			dao.add_relationship(entity.id_user, *ids_relationships)



if __name__ == "__main__":
	create_default_db()
