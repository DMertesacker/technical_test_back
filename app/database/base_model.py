class BaseModel:
	__tablename__ = ''
	__mapper__ = None

	@property
	def pk(self):
		try:
			return self.__mapper__.primary_key[0].name
		except:
			return ''