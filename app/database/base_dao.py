from app import db


class BaseDAO:

	def __init__(self):
		self.entity = self.model()

	@property
	def session(self):
		return db.session

	@property
	def model(self):
		raise NotImplementedError

	@property
	def pk(self):
		return self.entity.pk

	def create_instance(self):
		return self.model()

	def get_all(self):
		return self.entity.query.all()

	def get_by_pk(self, candidate):
		filter_query = dict([(self.pk, candidate)])
		return self.entity.query.filter_by(**filter_query).first()

	def get(self, first=False, **kwargs):
		res = self.entity.query.filter_by(**kwargs)
		if first:
			return res.first()
		return res.all()
