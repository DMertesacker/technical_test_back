# Instructions to start the project locally

## Create database

Before creating the database, you first have to create two schemas in a local database. algorath and test 

go to app/database/_scripts and run the create_db.py script

### `python3 create_db.py`

if you don't have the project set in your pythonpath, remember to add it before running the command

### `PYTHONPATH=/your/local/path/technical_test_back python3 create_db.py`

## Create Enviroment

Once the database is created, create the virtual environment in python 3.8

### `python3 -m venv algorath-env.`
### `algorath-env/Scripts/activate. bat.`
### `source algorath-env/bin/activate.`

## Install Libraries

inside the virtual environment, install the libraries of the requirements.txt

### `pip3 install -r requirements`

## Run services

run the wsgi.py script

## Run Test

in the terminal, inside the root of the project, run:

### `python3 -m unittest discover`
