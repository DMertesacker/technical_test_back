import unittest

from app import app, db
from app.database._scripts.create_db import create_default_db
from app.database.user.daos.user import UserDAO
from app.database.user.daos.user_relationship import UserRelationshipDAO
from config import Config
from test.custom_assertions import CustomAssertions


class TestUsers(unittest.TestCase, CustomAssertions):

	@property
	def use_testing_db(self):
		return app.config['SQLALCHEMY_DATABASE_URI'] == 'mysql+pymysql://root:@127.0.0.1/test'

	def setUp(self):
		app.config['SQLALCHEMY_DATABASE_URI'] = Config.SQLALCHEMY_DATABASE_URI_TEST
		app.config['TESTING'] = True
		app.app_context().push()
		self.app = app.test_client()
		if self.use_testing_db:
			create_default_db()

	def tearDown(self):
		if self.use_testing_db:
			db.session.remove()
			db.drop_all()

	def _check_api_response(self, response):
		self.assertTrue(response.is_json)
		self.assertEqual(response.status_code, 200)
		return response.get_json()

	def _get_call(self, url, *keys):
		response = self.app.get(url)
		data = self._check_api_response(response)

		self.assertIsList(data)
		for d in data:
			self.assertIsDict(d)
			self.assertNotEmptyDict(d)
			self.assertDictHasKeys(d, [*keys])
		return data

	def _post_call(self, url, post_data, *keys):
		response = self.app.post(url, json=post_data)
		data = self._check_api_response(response)

		self.assertIsList(data)
		for d in data:
			self.assertIsDict(d)
			self.assertNotEmptyDict(d)
			self.assertDictHasKeys(d, [*keys])
		return data

	def test_get_all_users(self):
		self._get_call('/api/user/all-users', 'userID', 'name', 'relNames', 'relIDs')

	def test_get_user_options(self):
		self._get_call('/api/user/user-options', 'value', 'label')

	def test_create_user(self):
		new_name = 'Steve'
		new_rels = [1, 2, 3, 4, 5]
		data = self._post_call('api/user/new', dict(
			name=new_name, rels=new_rels
		), 'userID', 'name', 'relNames', 'relIDs')

		for d in data:
			self.assertEqual(d['name'], new_name)
			for rel in d['relIDs']:
				self.assertIn(rel, new_rels)

	def test_update_user(self):
		id_updated = 1
		new_name = 'Steve'
		new_rels = [5, 6, 7, 8]
		data = self._post_call('/api/user/update', dict(
			id_user=id_updated, name=new_name, rels=new_rels
		), 'userID', 'name', 'relNames', 'relIDs')

		for d in data:
			self.assertEqual(d['name'], new_name)
			for rel in d['relIDs']:
				self.assertIn(rel, new_rels)

	def test_delete_user(self):
		id_delete = 2
		response = self.app.delete('/api/user/delete', json=dict(
			id_user=id_delete
		))

		self._check_api_response(response)
		entity = UserDAO().get_by_pk(id_delete)
		self.assertIsNone(entity)

		rels = UserRelationshipDAO().get(id_user=id_delete)
		self.assertEqual(len(rels), 0)

	def test_statistics(self):
		response = self.app.get('/api/user/statistics')
		data = self._check_api_response(response)

		self.assertIsDict(data)
		self.assertNotEmptyDict(data)
		self.assertDictHasKeys(data, ['userWithRels', 'userWithoutRels', 'top'])

		for d in data['top']:
			self.assertIsDict(d)
			self.assertNotEmptyDict(d)
			self.assertDictHasKeys(d, ['name', 'num'])