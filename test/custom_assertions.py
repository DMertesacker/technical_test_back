

class CustomAssertions:

    def assertIsList(self, data, msg=None):
        if msg is None:
            msg = f'{data} is not List'

        if not isinstance(data, list):
            if msg is None:
                msg = f'{data} is not List'
            raise AssertionError(msg)

    def assertIsDict(self, data, msg=None):
        if msg is None:
            msg = f'{data} is not Dict'

        if not isinstance(data, dict):
            raise AssertionError(msg)

    def assertNotEmptyDict(self, data, msg=None):
        if msg is None:
            msg = f'Dict {data} its Empty'

        if not data:
            raise AssertionError(msg)

    def assertDictHasKeys(self, data, keys, msg=None):
        myKeys = data.keys()

        for k in keys:
            if k not in myKeys:
                raise AssertionError(
                    msg
                    if msg is not None else
                    f'Key {k} Not found!'
                )

